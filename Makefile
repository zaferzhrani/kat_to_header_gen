all:
	@mkdir build
	@gcc src/main.c -o build/KAT_to_header
	@build/KAT_to_header KAT/PQCkemKAT_3168.rsp


.PHONY:clean
clean:
	@-rm -rf build

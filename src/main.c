#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#define MAX_KAT_FILE 102400

#define MAX_HEADER_FILE 1048576
#define    MAX_MARKER_LEN        50
#define    MAX_LINE        8000
#define KAT_SUCCESS          0
#define KAT_FILE_OPEN_ERROR -1
#define KAT_DATA_ERROR      -3
#define KAT_CRYPTO_FAILURE  -4

int FindMarker(FILE *infile, const char *marker);

int ReadHex(FILE *infile, unsigned char *A, int Length, char *str);

void fprintBstr(FILE *fp, char *S, unsigned char *A, unsigned long long L);

void DumpHex(const void *data, size_t size);

uint8_t kat_req_array[MAX_KAT_FILE];

uint8_t header_file_content[MAX_HEADER_FILE];

uint8_t seed[960];

uint8_t expected_key[320];

char dist_header_path[100];

int main(int argc, char *argv[])
{

    if (argc > 2 || argc < 2)
    {

        printf("\nFail argument");
        exit(0);
    }
	
	
	memcpy(dist_header_path,argv[0],strlen(argv[0]));
	
	
	for(int i=0 ; i<strlen(argv[0]) ; i++){
		if(dist_header_path[i] == 47){ // 47 dec = '/'
			dist_header_path[i+1]='\0';
			break;
		}
	}
	

	
	strcat(dist_header_path,"KAT_rsp_gen.h\0");	
			


    FILE *fp_rsp = fopen(argv[1], "rb");

    if (fp_rsp == NULL)
    {

        printf("\ncan't read file");
        exit(0);
    }

    printf("----------------------------- Generate KAT_rsp_gen.h -----------------------------\n\n");

    FILE *header_file = fopen(dist_header_path, "w"); /*craet header file*/
    if (header_file == NULL)
    {

        printf("\ncan't creat header file");
        exit(0);
    }

    int count = 0;
    int done = 0;

    /*caculate count of KAT_req file*/
    while (!done)
    {

        if (!FindMarker(fp_rsp, "count = "))
        {
            done = 1;
        }
        else
        {
            count++;
        }

    }

    fseek(fp_rsp, 0L, SEEK_SET); /*seek back after calculate*/



    sprintf(header_file_content,
            "#ifndef KAT_RSP_GEN_H\n"

            "#define KAT_RSP_GEN_H\n\n"

            "#include <stdint.h>\n"

            "\n\n#define KEY_LENGTH 32"

            "\n\n#define SEED_SIZE KEY_LENGTH * 3\n"

            "struct kat_req_res{\n"

            "\t uint8_t* seed;\n"

            "\t uint8_t* expected_key;\n"

            "}kem_kat[%d];\n\n\0", count);


    fputs(header_file_content, header_file);

    done = 0;
    count = 0;

    do
    {
        if (!FindMarker(fp_rsp, "count = "))
        {
            /*this section will ecxcute last the loop*/

            fputs("\n\nvoid set_req_res(){\n\n",
                  header_file); /*creat c function to set seed arrays to array_of_ptr_for_seeds_arrays*/

            for (int i = 0; i < count; i++)
            {
                sprintf(header_file_content,
                        "kem_kat[%d].seed = seed_%d;\nkem_kat[%d].expected_key = expected_key_%d;\n\n\0", i, i, i, i);
                fputs(header_file_content, header_file);
            }

            fputs("}\n\n#endif //KAT_RSP_GEN_H\n", header_file);


            done = 1;

            break;
        }


        if (!ReadHex(fp_rsp, seed, 96, "seed = "))
        {
            printf("ERROR: unable to read 'seed' \n");
            exit(0);
        }


        sprintf(header_file_content, "uint8_t seed_%d[SEED_SIZE]={%s};\n\0", count, seed);
        seed[0] = 0;

        fputs(header_file_content, header_file);

        if (!ReadHex(fp_rsp, expected_key, 32, "ss = "))
        {
            printf("ERROR: unable to read 'ss' \n");
            exit(0);
        }


        sprintf(header_file_content, "uint8_t expected_key_%d[KEY_LENGTH]={%s};\n\n\0", count, expected_key);
        expected_key[0] = 0;

        fputs(header_file_content, header_file);

        count++;


    }
    while (!done);


    fclose(header_file);
    fclose(fp_rsp);

    printf("------------------------------ Generation successes ------------------------------\n");

    return 0;
}


//
// ALLOW TO READ HEXADECIMAL ENTRY (KEYS, DATA, TEXT, etc.)
//
//
// ALLOW TO READ HEXADECIMAL ENTRY (KEYS, DATA, TEXT, etc.)
//
int
FindMarker(FILE *infile, const char *marker)
{
    char line[MAX_MARKER_LEN];
    int i, len;
    int curr_line;

    len = (int) strlen(marker);
    if (len > MAX_MARKER_LEN - 1)
    {
        len = MAX_MARKER_LEN - 1;
    }

    for (i = 0; i < len; i++)
    {
        curr_line = fgetc(infile);
        line[i] = curr_line;
        if (curr_line == EOF)
        {

            return 0;
        }
    }
    line[len] = '\0';

    while (1)
    {
        if (!strncmp(line, marker, len))
        {
            return 1;
        }

        for (i = 0; i < len - 1; i++)
        {
            line[i] = line[i + 1];
        }
        curr_line = fgetc(infile);
        line[len - 1] = curr_line;
        if (curr_line == EOF)
        {
            return 0;
        }
        line[len] = '\0';
    }

    // shouldn't get here
    return 0;
}

//
// ALLOW TO READ HEXADECIMAL ENTRY (KEYS, DATA, TEXT, etc.)
//
int
ReadHex(FILE *infile, unsigned char *A, int Length, char *str)
{

    unsigned char seed_as_c_array[1480] = {0};
    uint8_t temp[6] = {0};

    if (Length == 0)
    {
        A[0] = 0x00;
        return 1;
    }

    if (FindMarker(infile, str))
    {
        fgets(A, Length * 2 + 1, infile);
    }

    // DumpHex(A,strlen(A));


    for (int i = 0; i < Length * 2; i = i + 2)
    {
        if (i < Length * 2 - 2)
        {
            sprintf(temp, "0x%c%c,\0", A[i], A[i + 1]);
        }
        else
        {
            sprintf(temp, "0x%c%c\0", A[i], A[i + 1]);
        }

        strcat(seed_as_c_array, temp);
    }


    memcpy(A, seed_as_c_array, strlen(seed_as_c_array));
    return 1;

}


void DumpHex(const void *data, size_t size)
{
    char ascii[17];
    size_t i, j;
    ascii[16] = '\0';
    for (i = 0; i < size; ++i)
    {
        printf("%02X ", ((unsigned char *) data)[i]);
        if (((unsigned char *) data)[i] >= ' ' && ((unsigned char *) data)[i] <= '~')
        {
            ascii[i % 16] = ((unsigned char *) data)[i];
        }
        else
        {
            ascii[i % 16] = '.';
        }
        if ((i + 1) % 8 == 0 || i + 1 == size)
        {
            printf(" ");
            if ((i + 1) % 16 == 0)
            {
                printf("|  %s \n", ascii);
            }
            else if (i + 1 == size)
            {
                ascii[(i + 1) % 16] = '\0';
                if ((i + 1) % 16 <= 8)
                {
                    printf(" ");
                }
                for (j = (i + 1) % 16; j < 16; ++j)
                {
                    printf("   ");
                }
                printf("|  %s \n", ascii);
            }
        }
    }
}